1.  Cross_lingula_chart.py: vẽ biểu đồ so sánh acc khi xóa nodes và xóa edges theo tỉ lệ 0.1 đến 0.5 của tất cả các model
2.  Train_test.py: vẽ biểu đồ acc khi train_ratio từ 0.3 đến 0.9 của từng supervised model
3.  Del_atts.py: vẽ biểu đồ so sánh acc khi xóa attributes của các model sử dụng attributes
4.  Dims_chart.py:  vẽ biểu đồ thay đổi acc của EMGCN khi dim thay đổi
5.  Time_chart.py: vẽ biểu đồ tổng thời gian chạy của các model, Trong Time_chart có số liệu của thời gian chạy khi train_ratio =  0.3, 0.6, 0.9 của các supervised model ứng với từng datasets
6.  script: python Time_chart.py --language NAME  #ở đây NAME = all khi output là biểu đồ thời gian cho các thuật toán với 3 dataset tại 0.3 train_ratio, NAME = zh_en (hoặc ja_en, fr_en) là biểu đồ thời gian so sánh thời gian chạy của các thuật toán trên từng dataset ứng với train_ratio 0.3, 0.6, 0.9