import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
from pylab import *
import json
import os
import pdb
def get_data(file_path):
    text = open('cross_lingula/{}'.format(file_path),'r').read().split()
    ar = np.asarray(text, dtype=np.float64).reshape(7,6)
    ar = ar.T
    #x = np.delete(x,3,0)


    return ar

def line_chart(models, data_matrix, x_label, y_label, title, xpoints, higher_models = [], name=None, maxx=1.2):
    styles = ['s', 'd', '^', 'x', '*','v']
    #styles = ['o', 's', 'v', '*']
    line_styles = ['-', '--', '-', '-.', ':','-','--']
    # styles = ['o-', '>--', 's-', 'd-.', '^:', 'x-', '*-', 'v-']
    
    colors = ['#009933', '#33cc33', '#66ff66', '#99ff99', '#ffffff','#0033ff']
    barwith = 0.1
    ax1 = plt.subplot(111)

    num_models = data_matrix.shape[0]
    num_x_levels = data_matrix.shape[1]\
        
    assert num_models == len(models), "Number of model must equal to data matrix shape 0"


    lns1 = []
    b1 = ax1.bar(0, 0.8653,width=0.3,color = 'none', align='center',  hatch="...",edgecolor='#000000')
    lns1.append(b1)
    for i, model in enumerate(models):
        if model not in higher_models:
            line = data_matrix[i]
            x = np.arange(num_x_levels)+1
            fillstyle = 'none'
            if i > 3:
                fillstyle = 'full'
            lni, = ax1.plot(x, line, marker=styles[i % len(styles)], markersize=10, color='k', label=models[i], markevery=1, fillstyle=fillstyle)
            lns1.append(lni)

    ax1.set_xlabel(x_label, fontsize = 22)
    ax1.set_ylabel(y_label, fontsize = 22)
    ax2 = None
    lns2 = []
    count = 0

    for i, model in enumerate(models):
        if model in higher_models:
            line = data_matrix[i]
            x = np.arange(num_x_levels)
            if ax2 is None:
                ax2 = ax1.twinx()
            ln2 = ax2.bar(x + count * barwith, line, width=barwith, color = colors[count],  edgecolor='k', label=model, alpha = 0.5)
            lns2.append(ln2)
            count += 1


    plt.xticks(np.arange(len(xpoints)), xpoints, fontsize = 22)
    plt.yticks(np.arange(0, 1.1, step=0.2), fontsize = 22)
    ax1.set_xlim(-0.7, len(xpoints) + .3 - 1)
    ax1.set_ylim(-0.05, maxx + .22)
    # ypoints = np.arange(0, 1.1, 0.2)
    # plt.yticks(np.arange(len(ypoints)), ypoints, fontsize = 16)

    if ax2 is not None:
        ax2.set_xlim(-0.5, len(xpoints) + .5 - 1)
        ax2.set_ylim(0, 0.7)
        ax2.set_yticks(np.arange(0, 0.6, 0.1))
        ax2.tick_params('y', colors='green')

    ax1.grid(True)
    box = ax1.get_position()
    ax1.set_position([box.x0 + 0.05, box.y0 + 0.05, box.width , box.height])

    plt.legend(lns1,('EMGCN','BootEA', 'GCNA','RDGCN','KGM','JAPE', 'MuGNN'),ncol = 4,borderaxespad = 0.2, columnspacing = 0.75, fontsize=12.65)
    plt.savefig(name)
    plt.close()




if __name__ == "__main__":
    models = ['BootEA', 'GCNA','RDGCN','KGM','JAPE', 'MuGNN']
    mode = 'train_ratio'
    dataset = 'zh_en'
    xpoint_del_nodes = {"xp": ["0","0.3","0.4", "0.5", "0.6","0.7","0.8","0.9"], "xlabel": "Train Ratio"}
    #xpoint_del_edges = {"xp": ["0","0.1", "0.2", "0.3","0.4","0.5"], "xlabel": "Relations removal ratio"}
    xpoints = {'train_ratio': xpoint_del_nodes}
    ylabel = "Success@1"
    maxx = 1.05

    name = mode + '-' + dataset
    accs = get_data(name)
    print(accs.shape)
    xticks = xpoints[mode]["xp"]
    xlabel = xpoints[mode]["xlabel"]
    line_chart(models = models, data_matrix=accs, \
        x_label=xlabel, y_label=ylabel, title=name, xpoints=xticks, higher_models=[], name='{}.png'.format(name), maxx=maxx)
            