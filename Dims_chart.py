import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
from pylab import *
import json
import os
import pdb
from pylab import *

def get_data(file_path):
    text = open(file_path,'r').read().split()
    return np.asarray(text, dtype=np.float64).reshape(3,5)

def line_chart(models, data_matrix, x_label, y_label, title, xpoints, higher_models = [], name=None, maxx=1.1):
    
    barwith = 0.1
    #ax1 = plt.subplot(111)
    ax1 = plt.subplots(figsize=(12, 7))[1]
    #pdb.set_trace()
    num_models = data_matrix.shape[0]
    num_x_levels = data_matrix.shape[1]
        
    assert num_models == len(models), "Number of model must equal to data matrix shape 0"


    lns1 = []
    for i, model in enumerate(models):
        if model not in higher_models:
            line = data_matrix[i]
            x = np.arange(num_x_levels)
            
            if i > 3:
                fillstyle = 'full'
            lni, = ax1.plot(x, line, marker='o', markersize=14, label=models[i], markevery=1,linewidth=4,fillstyle = 'full')
            lns1.append(lni)

    ax1.set_xlabel(x_label, fontsize = 30)
    ax1.set_ylabel(y_label, fontsize = 30)

    plt.xticks(np.arange(len(xpoints)), xpoints, fontsize = 30)
    plt.yticks(np.arange(0.6, 1.09, step=0.1), fontsize = 30)
    ax1.set_xlim(-0.3, len(xpoints) + .3 - 1)
    ax1.set_ylim(0.58, maxx )
    # ypoints = np.arange(0, 1.1, 0.2)
    # plt.yticks(np.arange(len(ypoints)), ypoints, fontsize = 16)

    plt.gca().yaxis.grid(True,linewidth=1)
    box = ax1.get_position()
    ax1.set_position([box.x0+0.02, box.y0+0.04, box.width, box.height])
    plt.legend(ncol = 3,fontsize = 25,loc = 4)
    plt.savefig(name)
    plt.close()




if __name__ == "__main__":
    models = ['zh_en', 'ja_en','fr_en']
    modes = ['del_edges', 'del_nodes']
    datasets = ['BN','Econ-mahindas','Email-univ']
    xpoint_del_nodes = {"xp": ["0.1", "0.2", "0.3", "0.4", "0.5"], "xlabel": "Nodes removal ratio"}
    xpoint_del_edges = {"xp": ["0.1", "0.2", "0.3", "0.4", "0.5"], "xlabel": "Edges removal ratio"}
    xpoints = {'del_edges': xpoint_del_edges, 'del_nodes': xpoint_del_nodes}
    ylabel = "Success@1"
    maxx = 1.05
    
    file_name = "dim.txt"
    
    accs = get_data(file_name)
    xticks = ['10','50','100','200','500']
    xlabel = 'Embedding dimention'
    line_chart(models = models, data_matrix=accs, \
        x_label=xlabel, y_label=ylabel, title='name', xpoints=xticks, higher_models=[], name='Dim6.png', maxx=maxx)
        